import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ReplaySubject} from 'rxjs/ReplaySubject';
import {createWorker} from "typed-web-workers";

interface WeatherJson {
  t: string;
  v: number;
}

export enum WEATHER_URLS {
  TEMPERATURE = './assets/testData/temperature.json',
  PRECIPITATION = './assets/testData/precipitation.json'
}

@Injectable()
export class MeteoService {
  public temperatureSubject: ReplaySubject<MessageEvent>;
  private subjects = new Map<string, ReplaySubject<MessageEvent>>();

  constructor(private http: HttpClient) {
    this.temperatureSubject = new ReplaySubject(1);
  }

  private webWorker = createWorker(
    (dataUrl: { weathers: Array<WeatherJson>, url: string },
     callback: (_: any) => void) => {
      const weatherMap = new Map<number, Array<number>>();
      const averageWeatherMap = new Map<number, number>();

      dataUrl.weathers.forEach(weather => {
        const year = new Date(weather.t).getFullYear();
        if (!weatherMap.has(year)) {
          weatherMap.set(year, []);
        }
        weatherMap.set(year, weatherMap.get(year).concat(weather.v));
      });
      weatherMap.forEach((weather, year) => {
        averageWeatherMap.set(year, Math.round(weather.reduce((previous, current) => previous + current) / weather.length * 10000) / 10000);
      });
      return callback({url: dataUrl.url, averageWeatherMap: averageWeatherMap});
    },
    (result) => {
      this.subjects.get(result.url).next(result.averageWeatherMap);
    }
  );

  fetchWeather(url: string): ReplaySubject<any> {
    if (!this.subjects.has(url)) {
      this.subjects.set(url, new ReplaySubject(1));
      this.http.get<WeatherJson[]>(url)
        .subscribe(result => {
          this.webWorker.postMessage({weathers: result, url: url});
        });
    }
    return this.subjects.get(url);
  }
}
