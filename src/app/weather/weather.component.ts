import {Component, Input, OnInit} from '@angular/core';
import {MeteoService} from "../meteo-service.service";

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {
  @Input() dataUrl: string = '';
  @Input() label: string = '';

  public years: Array<string>;
  public fromYear: string;
  public toYear: string;
  public temperatureData;
  public lineChartData: Array<any>;
  public lineChartLabels: Array<any>;
  public lineChartOptions: any = {
    legend: {
      display: false,
    },
    elements:
      {
        point:
          {
            radius: 3
          }
      },
    maintainAspectRatio: false
  };
  public lineChartColors: Array<any> = [
    {
      backgroundColor: 'rgba(232, 234, 246, .5)',
      borderColor: '#7986CB',
      pointBackgroundColor: '#5C6BC0',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148, 159, 177, .8)'
    }
  ];

  constructor(private meteoService: MeteoService) {
  }

  ngOnInit() {
    this.lineChartData = [{data: [], label: this.label}];
    this.meteoService.fetchWeather(this.dataUrl).subscribe(data => {
      this.temperatureData = data;
      this.years = Array.from(data.keys());
      this.fromYear = this.years[0];
      this.toYear = this.years[this.years.length - 1];
      this.renderChart();
    });
  }

  renderChart() {
    const years = this.years.filter(year => year >= this.fromYear && year <= this.toYear);
    this.lineChartData[0].data = years.map(year => {
      return this.temperatureData.get(year);
    });
    this.lineChartLabels = years;
  }
}
