import {Component, OnInit} from '@angular/core';
import {WEATHER_URLS} from "../meteo-service.service";

@Component({
  selector: 'app-precipitation-page',
  templateUrl: './precipitation-page.component.html'
})
export class PrecipitationPageComponent implements OnInit {
  public urlToPrecipitationJson: string;

  ngOnInit() {
    this.urlToPrecipitationJson = WEATHER_URLS.PRECIPITATION;
  }
}
