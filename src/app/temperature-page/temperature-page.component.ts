import {Component, OnInit} from '@angular/core';
import {WEATHER_URLS} from '../meteo-service.service';

@Component({
  selector: 'app-temperature-page',
  templateUrl: './temperature-page.component.html'
})
export class TemperaturePageComponent implements OnInit {
  public urlToTemperatureJson: string;

  ngOnInit() {
    this.urlToTemperatureJson = WEATHER_URLS.TEMPERATURE;
  }
}
