import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {MeteoService} from "./meteo-service.service";
import {HttpClientModule} from "@angular/common/http";
import {TemperaturePageComponent} from './temperature-page/temperature-page.component';
import {PrecipitationPageComponent} from './precipitation-page/precipitation-page.component';
import {RouterModule, Routes} from "@angular/router";
import {MatTabsModule, MatSelectModule, MatGridListModule, MatCardModule} from '@angular/material';
import {platformBrowserDynamic} from "@angular/platform-browser-dynamic";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ChartsModule} from "ng2-charts";
import { WeatherComponent } from './weather/weather.component';

const routes: Routes = [
  { path: '', component: TemperaturePageComponent },
  { path: 'precipitation', component: PrecipitationPageComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    TemperaturePageComponent,
    PrecipitationPageComponent,
    WeatherComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTabsModule,
    MatSelectModule,
    MatGridListModule,
    ChartsModule,
    MatCardModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    MeteoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

platformBrowserDynamic().bootstrapModule(AppModule);
